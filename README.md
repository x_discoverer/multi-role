# 多角色 2022.05.04已经同步到最新版本，请按下面的说明替换文件

#### 一、创建用户角色表 Sys_UserRole(sqlserver数据库直接执行此脚本，mysql pgsql自己修改下面的创建表的脚本)
#### CREATE TABLE Sys_UserRole
#### (
#### Id INT IDENTITY PRIMARY KEY,
#### UserId INT NOT NULL,
#### RoleId INT NOT NULL ,
#### [Enable] INT NOT NULL,
#### [CreateID] [int] NULL,
#### [Creator] nvarchar(255) NULL,
#### [CreateDate] [datetime] NULL,
#### [ModifyID] [int] NULL,
#### [Modifier] nvarchar(255) NULL,
#### [ModifyDate] [datetime] NULL,
#### )

#### 二、将用户角色表 Sys_UserRole在代码生成器中生成代码(与用户表一样，项目类库选择System)

#### 三、数据库修改：
#### 1、修改用户表sys_user字段Role_Id ,RoleName，直接执行下面修改sql
#### ALTER TABLE dbo.Sys_User ALTER COLUMN Role_Id INT 
#### ALTER TABLE dbo.Sys_User ALTER COLUMN RoleName NVARCHAR(50) 

#### 2、代码生成器页面，选中用户信息，点击同步表结构-》生成页面-》生成model（修改后报错不用管，接着替换下面的类）







#### 后台代码修改(都修改对应Partial文件夹下的类即可)
#### 升级方式(如果下面的类没有修改过，直接覆盖即可，如果有修改过，请对比下面的文件差异)
#### 1、ActionPermissionFilter.cs 
#### 2、DictionaryHandler.cs 
#### 3、RoleContext.cs
#### 4、Sys_MenuService.cs         ISys_MenuService .cs              Sys_MenuController.cs         
#### 5、Sys_UserService.cs           ISys_UserService .cs                Sys_UserController.cs        
#### 6、Sys_RoleService.cs           ISys_RoleService .cs                Sys_RoleController.cs         
#### 7、Logger.cs
#### 8、JwtHelper.cs
#### 9、UserInfo.cs
#### 10、UserContext.cs
#### 11、如果给用户添加或者删除了角色 ，用户需要退出重新登陆才能看到最新的权限

#### 前端代码修改
#### 升级方式(如果下面的类没有修改过，直接覆盖即可，如果有修改过，请对比下面的文件差异)
#### Sys_UserGridHeader.vue
#### Sys_User.js

#### 其他注意事项
#### 1、在用户管理页面，直接点右边操作->设置角色(用户需要重新登陆才能看到最新的角色权限,内置缓存了用户权限信息)
#### 2、如果使用的不是开发版，请将Partial文件夹下的SellOrderService.cs也覆盖下


